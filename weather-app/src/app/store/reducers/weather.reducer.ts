import { Weather } from '../../shared/weather.model';
import * as WeatherActions from '../actions/weather.actions';

export interface State {
  weather: Weather;
  //dateToday: Date;
  cityName: string

}

const initalState: State = {
  weather: Weather.prototype,
  cityName: ''
};

const loadInitalState = (): State => {
  const cityName = localStorage.getItem('city');
  if(cityName) {
    return {
      ...initalState,
      cityName: cityName
    } 
  }

} 

export function reducer(
  state = loadInitalState(),
  action: WeatherActions.WeatherActions
) {
  switch (action.type) {
    case WeatherActions.SET_WEATHER:
      return {
        ...state,
        weather: action.payload,
        cityName: action.payload.name
      }
      default:
        return state;
  }
}

