import { City } from '../../shared/city.model';
import * as CityActions from '../actions/city.actions';

export interface State {
  cities: City[];
  editedCity: City;
  editedCityIndex: number;
}

const initalState: State = {
  cities: JSON.parse(localStorage.getItem('cities')) ? JSON.parse(localStorage.getItem('cities')) : [],
  editedCity: null,
  editedCityIndex: -1,
};

export function reducer(state = initalState, action: CityActions.CityActions) {
  switch (action.type) {
    case CityActions.ADD_CITY:
      return {
        ...state,
        cities: [...state.cities, action.payload],
      };
    case CityActions.EDIT_CITY:
      const city = state.cities[state.editedCityIndex];
      const updatedCity = {
        ...city,
        ...action.payload,
      };
      const updatedCities = [...state.cities];
      updatedCities[state.editedCityIndex] = updatedCity;
      return {
        ...state,
        cities: updatedCities,
        editCity: null,
        editedCityIndex: -1,
      };
    case CityActions.DELETE_CITY:
      const citiesFilter = state.cities.filter((city) => {
        return city.cityId !== action.payload
      
      })

      //ovo ne sme ovde, privremeno resenje
      localStorage.setItem('cities', JSON.stringify(citiesFilter));
      return {
        ...state,
        cities: citiesFilter,
        editedCity: null,
        editedCityIndex: -1,
      };
    case CityActions.START_EDIT:
      return {
        ...state,
        editedCity: { ...state.cities[action.payload] },
        editedCityIndex: action.payload,
      };
    case CityActions.STOP_EDIT:
      return {
        ...state,
        editedCity: null,
        editedCityIndex: -1,
      };
    default:
      return state;
  }
}
