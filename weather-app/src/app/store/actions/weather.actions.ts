import { Action } from "@ngrx/store";
import { Weather } from "../../shared/weather.model";

export const SET_WEATHER = '[Weather] Set Weather';
export const FETCH_WEATHER = '[Weather] Fetch Weather';
export const FETCH_WEATHER_COORDS = '[Weather] Fetch Weather Coords'; 

export class SetWeather implements Action {
  readonly type = SET_WEATHER;

  constructor(public payload: Weather) {}
}

export class FetchWeather implements Action {
  readonly type = FETCH_WEATHER;

  constructor(public payload: string) {}
}

export class FetchWeatherCoords implements Action {
  readonly type = FETCH_WEATHER_COORDS;

  constructor(public lat: number, public lon: number) {}
}

export type WeatherActions = 
  | SetWeather
  | FetchWeather
  | FetchWeatherCoords;