import { Action } from '@ngrx/store'
import { City } from '../../shared/city.model';

export const ADD_CITY = '[City] Add City';
export const EDIT_CITY = '[City] Edit City';
export const DELETE_CITY = '[City] Delete City';
export const SET_CITIES = '[City] Set Cities';
export const START_EDIT = '[City] Start Edit';
export const STOP_EDIT = '[City] Stop Edit'

export class AddCity implements Action {
  readonly type = ADD_CITY;

  constructor(public payload: City) {}
}

export class EditCity implements Action {
  readonly type = EDIT_CITY;

  constructor(public payload: City) {}
}

export class DeleteCity implements Action {
  readonly type = DELETE_CITY;

  constructor(public payload: number) {}
}

export class SetCities implements Action {
  readonly type = SET_CITIES;

  constructor(public payload: City[]) {}
}

export class StartEdit implements Action{
  readonly type = START_EDIT;

  constructor(public payload: number) {}
}

export class StopEdit implements Action {
  readonly type = STOP_EDIT;
}


export type CityActions = 
  | AddCity
  | EditCity
  | DeleteCity
  | SetCities
  | StartEdit
  | StopEdit;