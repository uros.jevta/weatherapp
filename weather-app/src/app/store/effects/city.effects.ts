// import { Injectable } from '@angular/core';
// import { Actions, Effect, ofType } from '@ngrx/effects';
// import { switchMap, map, tap, withLatestFrom } from 'rxjs/operators';
// import { LocalstorageService } from '../../shared/localstorage.service';
// import * as CitiesActions from '../actions/city.actions';
// import * as appStore from '../../store/app.states'
// import { Store } from '@ngrx/store';

// @Injectable()
// export class CityEffects {
//   constructor(
//     private actions$: Actions,
//     private localStorageService: LocalstorageService,
//     private store$: Store<appStore.AppState>
//   ) {}

//   @Effect()
//   storeActions = this.actions$.pipe(
//     ofType<CitiesActions.DeleteCity>(CitiesActions.DELETE_CITY),
//     withLatestFrom(this.store$.select('cities')),
//     map(state => {
//       return state.map(state => {
//         state.cities.filter(city => {
//           return city.cityId !== action.payload
//         })
//       })
//     })
//     tap((cities) => {
//       console.log('effect for localStorage');
//       this.localStorageService.saveToLocalStorage('cities', cities);
//     }),
//     map(() => {

//     })
//   );
// } 
