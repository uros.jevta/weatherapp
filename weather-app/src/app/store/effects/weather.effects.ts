import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { switchMap, map, tap } from 'rxjs/operators';
import * as WeatherActions from '../actions/weather.actions';
import { WeatherService } from 'src/app/weather/weather.service';

@Injectable()
export class WeatherEffects {
  constructor(
    private actions$: Actions<WeatherActions.WeatherActions>,
    private httpClient: HttpClient,
    private weatherService: WeatherService
  ) {}
  @Effect()
  fetchWeather = this.actions$.pipe(
    ofType(WeatherActions.FETCH_WEATHER),
    switchMap((action) => {
      return this.weatherService.fetchWeatherByCityName(action.payload);
    }),
    map((weather) => {
      return new WeatherActions.SetWeather(weather);
    })
  );

  @Effect()
  fetchWeatherWithCoords = this.actions$.pipe(
    ofType(WeatherActions.FETCH_WEATHER_COORDS),
    switchMap((action) => {
      return this.weatherService.fetchWeatherByCoordiantes(
        action.lat,
        action.lon
      );
    }),
    tap((weather) => {
      localStorage.setItem('city', weather.name);
    }),
    map((weather) => {
      return new WeatherActions.SetWeather(weather);
    })
  );
}
