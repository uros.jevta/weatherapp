
import * as fromCities from '../store/reducers/city.reducer'
import * as fromWeather from '../store/reducers/weather.reducer'
import { ActionReducerMap } from '@ngrx/store'

export interface AppState {
  cities: fromCities.State
  weather: fromWeather.State
}

export const reducers = {
  cities: fromCities.reducer,
  weather: fromWeather.reducer
}