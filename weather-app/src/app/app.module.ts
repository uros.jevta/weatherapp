import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CityComponent } from './cities/city/city.component';
import { WeatherComponent } from './weather/weather.component';
import { CitiesModule } from './cities/cities.module';
import { WeatherService } from './weather/weather.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { WeatherModule } from './weather/weather.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AddCityDialogComponent } from './cities/add-city-dialog/add-city-dialog.component';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HeaderComponent } from './header/header.component';
import * as Sentry from '@sentry/angular';
import { HttpErrorInterceptor } from './shared/http-error.interceptor';
import { ConfirmDialogComponent } from './shared/confirm-dialog/confirm-dialog.component';
import { StoreModule } from '@ngrx/store';
import * as appStore from './store/app.states';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { WeatherEffects } from './store/effects/weather.effects';
import { AgmCoreModule } from '@agm/core';
//import {CityEffects} from './store/effects/city.effect'
 
//to do: import weatherEfects
@NgModule({
  declarations: [
    AppComponent,
    CityComponent,
    WeatherComponent,
    HeaderComponent,
    ConfirmDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CitiesModule,
    WeatherModule,
    AppRoutingModule,
    NoopAnimationsModule,
    FormsModule,
    FlexLayoutModule,
    StoreModule.forRoot(appStore.reducers),
    EffectsModule.forRoot([WeatherEffects]),
    StoreDevtoolsModule.instrument({
      name: 'NgRx Weather Demo App',
      logOnly: environment.production,
    }),
    AgmCoreModule.forRoot({
      apiKey: '',
    }),
  ],
  entryComponents: [AddCityDialogComponent],
  providers: [ 
    WeatherService,
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        disableClose: true,
        hasBackdrop: true,
        autoFocus: false,
        data: {},
      },
    },
    {
      provide: ErrorHandler,
      useValue: Sentry.createErrorHandler({
        showDialog: false,
      }),
    },
    {
      provide: HTTP_INTERCEPTORS,

      useClass: HttpErrorInterceptor,

      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
