export class City {
  constructor(
    public cityId: number,
    public cityName: string,
    public latitude: number,
    public longitude: number,
    public population: number,
    public surfaceArea:  number,
    public imagePath: string
  ) {}
}