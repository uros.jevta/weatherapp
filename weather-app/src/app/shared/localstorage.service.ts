import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LocalstorageService {
  constructor() {}

  saveToLocalStorage(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  getFromLocalStorage(value) {
    return of(JSON.parse(localStorage.getItem(value)));
  }
 
  getCityFromLocalStorage() {
    return of(localStorage.getItem('city'));
  }
}
 