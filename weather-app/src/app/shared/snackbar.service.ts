import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(private snackBar: MatSnackBar) {
  }

  info(message, title = 'Close') {
    this.open(message, title, 'Some info');
  }

  success(message, title = 'Close') {
    this.open(message, title, 'City added');
  }

  warning(message, title = 'Close') {
    this.open(message, title, 'warning');
  }

  danger(message, title = 'Close') {
    this.open(message, title, 'City deleted');
  }

  private open(message, title = 'Close', classes = '') {
    this.snackBar.open(message, title, {
      duration: 5000,
      verticalPosition: 'bottom',
      horizontalPosition: 'left',
      panelClass: classes
    }); 
  }
}
