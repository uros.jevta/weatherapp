import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Weather } from '../shared/weather.model';
import { from, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { City } from '../shared/city.model';
import { LocalstorageService } from '../shared/localstorage.service';

@Injectable({ providedIn: 'root' })
export class WeatherService {
  private weather: Weather;

  constructor(
    private httpClient: HttpClient,
    private localStorageService: LocalstorageService
  ) {}

  fetchWeatherByCityName(cityName = 'Kragujevac') {
    try {
      return this.httpClient.get<Weather>(
        'https://api.openweathermap.org/data/2.5/weather?q=' +
          cityName +
          '&units=metric' +
          '&appid=' +
          environment.weatherAPIkey 
      );
    } catch (error) {
      console.log(error);
    }
  }

  setWeather(weather: Weather) {
    this.weather = weather;
  }

  getWeather() {
    return this.weather;
  }

  fetchWeatherByCoordiantes(lat, lon) {
    try {
      return this.httpClient.get<Weather>(
        `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=metric&appid=${environment.weatherAPIkey}`
      );
    } catch (error) {
      console.log(error);
    }
  }

  createCityFromWeather(lat, lon, weather: Weather) {
    let cities: City[];
    this.localStorageService
      .getFromLocalStorage('cities')
      .subscribe((citiesFromStorage) => {
        cities = citiesFromStorage;
      });
    let newCity: City;

    let newId = cities.length ? cities[cities.length - 1].cityId + 1 : 1;

    newCity = new City(
      newId,
      weather.name,
      weather.coord.lat,
      weather.coord.lon,
      10,
      10,
      ''  
    ); 

    //if cities is empty array or doesn't exist
    if (!cities) {
      cities = [];
    }

    // if newCity is null
    if(newCity) {
      cities.push(newCity);
    }

    this.localStorageService.saveToLocalStorage('cities', cities);

    return newCity;
  }
}
