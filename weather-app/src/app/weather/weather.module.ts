import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';


const MATERIAL_MODULES = [
  MatCardModule,
  MatButtonModule,
  MatProgressSpinnerModule
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MATERIAL_MODULES 
  ],
  exports: [
    MATERIAL_MODULES 
  ]
})
export class WeatherModule { }
