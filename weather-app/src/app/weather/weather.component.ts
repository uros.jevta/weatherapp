import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { WeatherService } from './weather.service';
import { Weather } from '../shared/weather.model';
import { Subscription, Observable } from 'rxjs';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { LocalstorageService } from '../shared/localstorage.service';
import * as appStore from '../store/app.states';
import { Store } from '@ngrx/store';
import * as WeatherActions from '../store/actions/weather.actions';
import * as CityActions from '../store/actions/city.actions';
import { MouseEvent as AGMMouseEvent } from '@agm/core';

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css'],
})
export class WeatherComponent implements OnInit, OnDestroy {
  weather: Weather;
  dateToday: Date;
  formattedDate: string;
  sub: Subscription;

  lat = 51.678418;
  lng = 7.809007;

  constructor(
    private weatherService: WeatherService,
    private route: ActivatedRoute,
    private localStorageService: LocalstorageService,
    private store: Store<appStore.AppState>
  ) {}

  ngOnInit(): void {
    this.initWeather();
    this.initStates();
  }

  initStates() {
    this.store.select('weather').subscribe((state) => {
      this.lat = state?.weather?.coord?.lat
        ? state?.weather?.coord?.lat
        : this.lat;
      this.lng = state.weather.coord?.lon ? state.weather.coord?.lon : this.lng;
      this.weather = state.weather;
      this.dateToday = new Date(this.weather.dt * 1000);
      this.formattedDate = moment(this.dateToday).format('DD. MMMM YYYY.');
    });
  }

  initWeather() {
    let cityName;
    this.localStorageService.getCityFromLocalStorage().subscribe((city) => {
      cityName = city ? city : 'Kragujevac';
    });
    this.store.dispatch(new WeatherActions.FetchWeather(cityName));
  }

  getWeather() {
    this.sub = this.weatherService
      .fetchWeatherByCityName()
      .subscribe((fetchedWeather) => {
        this.weather = fetchedWeather;
      });
  }

  ngOnDestroy() {}

  mapClicked($event: AGMMouseEvent) {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;

    this.store.dispatch(
      new WeatherActions.FetchWeatherCoords(this.lat, this.lng)
    );
    //this.initWeather();
    let city = this.weatherService.createCityFromWeather(
      this.lat,
      this.lng,
      this.weather
    );
    if (city) {
      this.store.dispatch(new CityActions.AddCity(city));
    }
  }
}
