import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTableModule} from '@angular/material/table';

import { CitiesComponent } from './cities.component';
import {MatButtonModule} from '@angular/material/button';
import { AddCityDialogComponent } from './add-city-dialog/add-city-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatSortModule } from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

const MATERIAL_MODULES = [
  MatTableModule,
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatSortModule,
  MatPaginatorModule,
  MatSnackBarModule,
  MatProgressSpinnerModule
]
 

@NgModule({ 
  declarations: [
    CitiesComponent,
    AddCityDialogComponent
  ], 
  imports: [
    ...MATERIAL_MODULES,
    CommonModule,
    ReactiveFormsModule, 
    FormsModule
  ],   
  exports: [
    ...MATERIAL_MODULES,
    ReactiveFormsModule,
    FormsModule  
  ]
})
export class CitiesModule { }
