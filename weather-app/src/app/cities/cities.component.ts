import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { City } from '../shared/city.model';
import { CitiesService } from './cities.service';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { AddCityDialogComponent } from './add-city-dialog/add-city-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { LocalstorageService } from '../shared/localstorage.service';
import { Subscription, Observable } from 'rxjs';
import { ConfirmDialogComponent } from '../shared/confirm-dialog/confirm-dialog.component';
import * as appStore from '../store/app.states';
import { Store } from '@ngrx/store';
import * as CityActions from '../store/actions/city.actions';
import { MatPaginator } from '@angular/material/paginator';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { SnackbarService } from '../shared/snackbar.service';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css'],
})
export class CitiesComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = [
    'cityId',
    'cityName',
    'latitude',
    'longitude',
    'population',
    'surfaceArea',
    'edit',
    'delete',
  ];
  sub: Subscription;
  sub2: Subscription;
  CitiesObservable = new Observable<City>();
  public dataSource = new MatTableDataSource<City>();
  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private citiesService: CitiesService,
    private dialog: MatDialog,
    private router: Router,
    private localStorageService: LocalstorageService,
    private _snackBar: MatSnackBar,
    private store: Store<appStore.AppState>,
    private snackbarService: SnackbarService
  ) {}

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.store.select('cities').subscribe((store) => {
      this.dataSource.data = store.cities;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  addCityDialog() {
    const dialogConfig = new MatDialogConfig();

    let dialogRef = this.dialog.open(AddCityDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((city) => {
      if (city) {
        this.store.dispatch(new CityActions.AddCity(city));
      }
      //this.snackbarService.success('City added!', 'End now');
      this.activateSnackBar('City added!');
    }); 
  }

  onEdit(cityIdEdit: number, event) {
    event.stopPropagation();
    let cities: City[];
    this.sub2 = this.localStorageService
      .getFromLocalStorage('cities')
      .subscribe((citiesFromStorage) => {
        cities = citiesFromStorage;
      });
    let city: City = cities.find((city) => city.cityId === cityIdEdit);

    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = new City(
      city.cityId,
      city.cityName,
      city.latitude,
      city.longitude,
      city.population,
      city.surfaceArea,
      city.imagePath
    );

    let dialogRef = this.dialog.open(AddCityDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((city) => {
      if (city) {
        this.store.dispatch(new CityActions.EditCity(city));
        this.store.dispatch(new CityActions.StopEdit());
        this.activateSnackBar('City edited!');
      }
    });
  }

  onDelete(cityId: number, event) {
    event.stopPropagation();

    const dialogConfig = new MatDialogConfig();
    let dialogRef = this.dialog.open(ConfirmDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.store.dispatch(new CityActions.DeleteCity(cityId));
        this.activateSnackBar('City deleted!');
      }
    });
  }

  doFilter(value: string) {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  changeWeatherForCity(row: City) {
    localStorage.setItem('city', row.cityName);
    this.router.navigate(['/weather'], { queryParams: { city: row.cityName } });
  }

  activateSnackBar(msg: string) {
    this._snackBar.open(msg, 'End now', {
      duration: 5000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  ngOnDestroy() {
    this.sub ? this.sub.unsubscribe() : '';
    this.sub2 ? this.sub2.unsubscribe() : '';
  }
}
