import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { City } from 'src/app/shared/city.model';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { find } from 'rxjs/operators';
import { CitiesService } from '../cities.service';
import { LocalstorageService } from 'src/app/shared/localstorage.service';
import { Subscription } from 'rxjs';
import * as appStore from '../../store/app.states'
import { Store } from '@ngrx/store';
import * as CityActions from '../../store/actions/city.actions'

@Component({
  selector: 'app-add-city-dialog',
  templateUrl: './add-city-dialog.component.html',
  styleUrls: ['./add-city-dialog.component.css'],
})
export class AddCityDialogComponent implements OnInit, OnDestroy {
  form: FormGroup;
  city: City;
  editMode: boolean = false;
  sub: Subscription;

  constructor(
    private dialogRef: MatDialogRef<AddCityDialogComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: City,
    private citiesService: CitiesService,
    private localStorageService: LocalstorageService,
    private store: Store<appStore.AppState>
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      cityName: ['', Validators.required],
      latitude: ['', Validators.required],
      longitude: ['', Validators.required],
      population: ['', Validators.required],
      surfaceArea: ['', Validators.required,
      ],
    });

    if (this.data) {
      this.editMode = true;
      this.form.patchValue(this.data);
    } else {
      this.editMode = false;
    } 
  }
 
  close() {
    this.dialogRef.close();
  }
  save() {
    let cities: City[];
    this.sub = this.localStorageService
      .getFromLocalStorage('cities')
      .subscribe((citiesFromStorage) => {
        cities = citiesFromStorage;
      });
    let newCity: City;
    if (this.editMode) {
      let city = cities.find((singleCity) => {
        return singleCity.cityId === this.data.cityId;
      }); 
      let index = cities.indexOf(city);
      let cityId = city.cityId;

      this.store.dispatch(new CityActions.StartEdit(index));

      newCity = this.createNewCityObject(cityId);

      cities[index] = {
        ...cities[index],
        cityName: newCity.cityName,
        latitude: newCity.latitude,
        longitude: newCity.longitude,
        population: newCity.population,
        surfaceArea: newCity.surfaceArea,
        imagePath: newCity.imagePath,
      }; 
  


      this.localStorageService.saveToLocalStorage('cities', cities);
      this.dialogRef.close(newCity);
    } else {
      let newId = cities?.length ? cities[cities?.length - 1]?.cityId + 1 : 1;
      newCity = this.createNewCityObject(newId);
      if (!cities) {
        cities = [];
      }
      cities.push(newCity);

      this.localStorageService.saveToLocalStorage('cities', cities);
      this.dialogRef.close(newCity);
    }
  }

  createNewCityObject(newId: number) {
    this.city = new City(
      newId,
      this.form.get('cityName').value,
      parseInt(this.form.get('latitude').value),
      parseInt(this.form.get('longitude').value),
      parseInt(this.form.get('population').value),
      parseInt(this.form.get('surfaceArea').value),
      ''
    );
    return this.city;
  }

  ngOnDestroy() {
    //this.sub.unsubscribe();
  }
}
