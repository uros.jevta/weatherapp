import { Injectable } from '@angular/core';
import { City } from '../shared/city.model';
import { LocalstorageService } from '../shared/localstorage.service';
import { of, Subscription } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CitiesService {
  private cities: City[] = [
    // { cityId: 1, cityName: 'Kragujevac', latitude: 50, longitude: 100, population: 300000, surfaceArea: 25000, imagePath: ''},
    // { cityId: 2, cityName: 'Beograd', latitude: 50, longitude: 100, population: 300000, surfaceArea: 25000, imagePath: ''},
    // { cityId: 3, cityName: 'Nis', latitude: 50, longitude: 100, population: 300000, surfaceArea: 25000, imagePath: ''}
    // new City(1, 'Kragujevac', 50, 100, 300000, 25000, ''),
    // new City(2, 'Beograd', 50, 100, 300000, 25000, ''),
    // new City(3, 'Jagodina', 50, 100, 300000, 25000, ''),
  ];
  sub: Subscription;

  constructor(private localStorageService: LocalstorageService) {}

  getCities() {
    // this.cities = this.localStorageService.getFromLocalStorage('cities');
    // return this.cities;

    this.sub = this.localStorageService
      .getFromLocalStorage('cities')
      .subscribe((citiesFromStorage) => {
        this.cities = citiesFromStorage;
      });
    return of(this.cities); 
  }

  deleteCity(cityId: number) {
    let cityWithID = this.cities.find((city) => cityId === city.cityId);
    let index = this.cities.indexOf(cityWithID);
    this.cities.splice(index, 1);
    this.localStorageService.saveToLocalStorage('cities', this.cities);
  }

  addCity(city: City) {
    this.cities.push(city);
  }
}
